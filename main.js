console.log("hello world");



const Component2 = {
    props: ['val'],
    methods: {
        log(message) {
            console.log(message)
        }
    },
    data() {
        return {
            customData: 'test data'
        }
    },
    template: '<span @click="log(val)">{{customData}} {{val}}</span>'
}


const Component = {
    components: {
        Component2
    },
    props: ['val'],
    methods: {
        log(message) {
            console.log(message)
        }
    },
    data() {
        return {
            customData: 'test data'
        }
    },
    template: '<span @click="log(val)">{{customData}} {{val}} </span><Component2 val="sasasa" />'
}


var app = Vue.createApp({
    components: {
        Component
    },
    mounted(){
        console.log('mounted')
    },
    methods: {
        fetch() {
            console.log('test')
        }
    },
    template: `
        {{name}} {{lastname}}
        {{count}}
        <ul>
            <li @click="count++" v-for="todo in todos">{{ todo }}</li>
        </ul>
        <Component :val="count" />
    `,
    data() {
        return {
            count: 0,
            name: "World!",
            lastname: 'Ciccio',
            todos: ['todo 1', 'todo 2']
        };
    }
});

app.mount('#app');
